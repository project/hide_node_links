// The following line will prevent a JavaScript error if this file is included and vertical tabs are disabled.
Drupal.verticalTabs = Drupal.verticalTabs || {};

// Note the name here matches the name of the fieldset ID.
Drupal.verticalTabs.hide_node_links = function() {
  if ($('#edit-hide-node-links').size()) {
    if ($('#edit-hide-node-links').attr('checked')) {
      return Drupal.t('Hidden');
    }
    else {
      return Drupal.t('Visible');
    }
  }
  else {
    return '';
  }
}
